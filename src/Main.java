import classfile.*;

import java.util.*;
import java.util.function.BiFunction;

import static classfile.Aron.*;
import static classfile.Print.*;
import static classfile.Test.*;
import static classfile.MaxStack.*;

/**
 * KEY: all tests for Aron.java here
 *
 *
 */
public class Main {
    public static void main(String[] args) {
        test_drop_int();
        test_multiLongInt();
        test_addLongInteger();
        test_drop_list();
        test_power();
        tets_perfectSubstring();
        test_splitAtRecur();
        test_replaceSetOfString();
        test_splitStrWhenChar();
        test_isPowerOf2();
        test_multiLongList();
        test_geneMatrixRandomInt();
        test_dimArr2();
        test_matrixReplaceRow();
        test_matrixReplaceColumn();
        test_firstNonRepeatingChar();
        test_matrixSwapRow();
        test_firstKRepeatint();
        test_firstKRepeadouble();

        test0_takeArrEnd();
        test1_dropArrEnd();
        test2_partitionBinaryArray();
        test3_dropArrBoth();
        test_isSubstring();
        test_containsSubstr();
        test_substringIndex();
        test_Heap();
        test_heapSort();
        test_createBinNoImage();
        test_deleteMaxNode();
        test_deleteMinNode();
        test_flipBinTree();
        test_flipBinTreeMutable();
	test_isBSTRecurve();
	test1_isBSTRecurve();
	test_MaxStack();
	test_fillArrLeft();

	test_fillArrLeft();
	test_fillArrRight();
        test_addLongIntegerNew();
	test_multiLongInteger();
	test_removeAdjacentDuplicateChar();
	test_firstLastPosition();
        test_swap_arr();
        test_swapRow();
        test_swapCol();
        test_partition();
        test_fillListStr2d();
        test_insertIndexToRight();
        test_replaceIndexCol();
        test_splitWhileStr();
        test_splitIf();
        test_splitNStr();
        test_tableInteger();
        test_foldStrDrop();
        test_foldStrDrop2();
        test_splitMatch();
        test_splitRegexOne();
        test_listToArray2d();
        test_rotate_list();
        test_transpose_new();
        test_flipX();
        test_flipY();
        test_alignmentNonSPC();
        test_tableMask_table_tableBit();
        test_joinListStr();
        test_partitionWithCountSwap();
    }

    public static void test0_takeArrEnd() {
        beg();
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = takeArrEnd(2, arr);
            int[] expArr = {3, 4};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = takeArrEnd(1, arr);
            int[] expArr = {1};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = takeArrEnd(0, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = takeArrEnd(2, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        end();
    }

    public static void test1_dropArrEnd() {
        beg();
        {
            int[] arr = {1};
            int[] ret = dropArrEnd(0, arr);
            int[] expArr = {1};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrEnd(0, arr);
            int[] expArr = {1, 2};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrEnd(2, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrEnd(3, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = dropArrEnd(2, arr);
            int[] expArr = {1, 2};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = dropArrEnd(1, arr);
            int[] expArr = {1, 2, 3};
            t(ret, expArr);
        }
        end();
    }

    public static void test2_partitionBinaryArray() {
        beg();
        {
            int[] arr = {1, 1, 1};
            int[] ret = partitionBinaryArray(arr);
            int[] expArr = {0, 2};
            t(ret, expArr);
        }
        {
            int[] arr = {0, 1, 0};
            int[] ret = partitionBinaryArray(arr);
            int[] expArr = {-1, -1};
            t(ret, expArr);
        }
        {
            int[] arr = {0, 0, 0};
            int[] ret = partitionBinaryArray(arr);
            int[] expArr = {0, 2};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 0, 1, 0, 1, 0};
            int[] ret = partitionBinaryArray(arr);
            int[] expArr = {1, 4};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 0, 1, 0, 0, 1, 0};
            int[] ret = partitionBinaryArray(arr);
            int[] expArr = {1, 4};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 0, 1, 0, 0, 1, 0, 0};
            int[] ret = partitionBinaryArray(arr);
            int[] expArr = {-1, -1};
            t(ret, expArr);
        }
        end();
    }

    public static void test3_dropArrBoth() {
        beg();
        {
            int[] arr = {1};
            int[] ret = dropArrBoth(1, 1, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1};
            int[] ret = dropArrBoth(1, 2, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2};
            int[] ret = dropArrBoth(1, 1, arr);
            int[] expArr = {};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3};
            int[] ret = dropArrBoth(1, 1, arr);
            int[] expArr = {2};
            t(ret, expArr);
        }
        {
            int[] arr = {1, 2, 3, 4};
            int[] ret = dropArrBoth(0, 1, arr);
            int[] expArr = {1, 2, 3};
            t(ret, expArr);
        }
        end();
    }

    public static void test_firstNonRepeatingChar() {
        beg();
        {
            String str = "a";
            String ret = firstNonRepeatingChar(str);
            t(ret, "a");
        }
        {
            String str = "aab";
            String ret = firstNonRepeatingChar(str);
            t(ret, "b");
        }
        {
            String str = "aba";
            String ret = firstNonRepeatingChar(str);
            t(ret, "b");
        }
        {
            String str = "abab";
            String ret = firstNonRepeatingChar(str);
            t(ret, "");
        }
        {
            String str = "abcabk";
            String ret = firstNonRepeatingChar(str);
            t(ret, "c");
        }
        end();
    }

    public static void test_matrixSwapRow() {
        beg();
        {
            int[][] mat = {
                    {1, 4},
                    {2, 5},
                    {3, 6}
            };
            int inx0 = 1;
            int inx1 = 2;
            int[][] newMat = matrixSwapRow(mat, inx0, inx1);
            int[][] expMat = {
                    {1, 4},
                    {3, 6},
                    {2, 5}
            };
            t(newMat, expMat);
        }
        end();
    }


    public static void test_matrixReplaceRow() {
        beg();
        {
            int[][] mat = {
                    {1, 2},
                    {1, 2},
                    {1, 2}
            };
            int[] arr = {3, 4};
            int[][] newMat = matrixReplaceRow(mat, arr, 1);
            int[][] expMat = {
                    {1, 2},
                    {3, 4},
                    {1, 2}
            };
            t(newMat, expMat);
        }

        end();
    }

    public static void test_firstKRepeatint() {
        beg();
        {
            int[] arr = {5, 3, 4};
            int n = firstKRepeatint(3, arr);
            t(n, 0);
        }
        {
            int[] arr = {3, 4};
            int n = firstKRepeatint(3, arr);
            t(n, 1);
        }
        {
            int[] arr = {3, 4, 3};
            int n = firstKRepeatint(3, arr);
            t(n, 1);
        }
        {
            int[] arr = {3, 3, 3};
            int n = firstKRepeatint(3, arr);
            t(n, 3);
        }
        {
            int[] arr = {0, 0, 3, 0, 0};
            int n = firstKRepeatint(0, arr);
            t(n, 2);
        }
        end();
    }

    public static void test_firstKRepeadouble() {
        beg();
        {
            double[] arr = {5.01, 3.01, 4};
            int n = firstKRepeatdouble(3.02, arr);
            t(n, 0);
        }
        {
            double[] arr = {3.01, 4};
            int n = firstKRepeatdouble(3, arr);
            t(n, 0);
        }
        {
            double[] arr = {3.01, 3.01, 3.02};
            int n = firstKRepeatdouble(3.01, arr);
            t(n, 2);
        }
        {
            double[] arr = {4.0, 4.0, 3};
            int n = firstKRepeatdouble(4, arr);
            t(n, 2);
        }
        {
            double[] arr = {3.02, 3.01, 3};
            int n = firstKRepeatdouble(3, arr);
            t(n, 0);
        }
        {
            double[] arr = {0, 0, 3, 0, 0};
            int n = firstKRepeatdouble(0, arr);
            t(n, 2);
        }
        end();
    }


    public static void test_matrixReplaceColumn() {
        beg();
        {
            int[][] mat = {
                    {1, 2},
                    {1, 2},
                    {1, 2}
            };
            int[] colArr = {3, 4, 5};
            int[][] newMat = matrixReplaceColumn(mat, colArr, 1);
            int[][] expMat = {
                    {1, 3},
                    {1, 4},
                    {1, 5}
            };
            t(newMat, expMat);
        }

        end();
    }

    public static void test_dimArr2() {
        beg();
        {
            Integer[][] arr = {{1}};
            Tuple<Integer, Integer> tup = dimArr2(arr);
            t(tup.x, 1);
            t(tup.y, 1);
        }
        {
            int[][] arr = {
                    {1, 2},
                    {1, 2},
                    {1, 2}
            };
            Tuple<Integer, Integer> tup = dimArr2(arr);
            t(tup.x, 3);
            t(tup.y, 2);
        }

        end();
    }

    public static void test_geneMatrixRandomInt() {
        beg();
        {
            int[][] mat = geneMatrixRandomInt(10, 3, 2);
            p(mat);
        }
        end();
    }

    public static void test_multiLongList() {
        beg();
        {
            List<Integer> ls = new ArrayList<>(Arrays.asList(1, 2, 3));
            List<Integer> ret = multiLongList(ls, ls);

            List<Integer> expList = list(1, 5, 1, 2, 9);
            t(ret, expList);
        }
        {
            List<Integer> ls = new ArrayList<>(Arrays.asList(0));
            List<Integer> ret = multiLongList(ls, ls);

            List<Integer> expList = list(0);
            t(ret, expList);
        }
        {
            List<Integer> ls = new ArrayList<>(Arrays.asList(0));
            List<Integer> ls2 = new ArrayList<>(Arrays.asList(109));
            List<Integer> ret = multiLongList(ls, ls2);

            List<Integer> expList = list(0);
            t(ret, expList);
        }
        end();
    }

    public static void test_subArray() {
        beg();
        {
            Integer[] arr = {1, 2, 3};
            int start = 0;
            int end = 0;
        }
        end();
    }


    public static void test_drop_int() {
        beg();
        {
            int[] arr = {1, 2, 3};
            int[] list = drop(1, arr);
            int[] expected = {2, 3};
            t(list, expected);
        }
        {
            int[] arr = {1, 2, 3};
            int[] list = drop(0, arr);
            int[] expected = {1, 2, 3};
            t(list, expected);
        }
        {
            int[] arr = {1, 2, 3};
            int[] list = drop(3, arr);
            int[] expected = {};
            t(list, expected);
        }
        end();
    }

    public static void test_drop_list() {
        beg();
        {
            List<Integer> ls = of(1, 2, 3);
            List<Integer> list = drop(0, ls);
            List<Integer> expected = of(1, 2, 3);
            t(list, expected);
        }
        {
            List<Integer> ls = of(1, 2, 3);
            List<Integer> list = drop(1, ls);
            List<Integer> expected = list(2, 3);
            t(list, expected);
        }
        {
            List<Integer> ls = of(1, 2, 3);
            List<Integer> list = drop(3, ls);
            List<Integer> expected = of();
            t(list, expected);
        }
        end();
    }


    public static void test_addLongInteger() {
        beg();
        {
            Integer[] a = {0};
            Integer[] b = {0};
            Integer[] expected = {0};
            Integer[] list = addLongInteger(a, b);
            t(list, expected);
        }
        {
            Integer[] a = {9};
            Integer[] b = {0};
            Integer[] expected = {9};
            Integer[] list = addLongInteger(a, b);
            t(list, expected);
        }
        {
            Integer[] a = {9, 0};
            Integer[] b = {0};
            Integer[] expected = {9, 0};
            Integer[] list = addLongInteger(a, b);
            t(list, expected);
        }
        {
            Integer[] a = {9, 0};
            Integer[] b = {9, 0};
            Integer[] expected = {1, 8, 0};
            Integer[] list = addLongInteger(a, b);
            t(list, expected);
        }
        {
            Integer[] a = {9, 9};
            Integer[] b = {9, 0};
            Integer[] expected = {1, 8, 9};
            Integer[] list = addLongInteger(a, b);
            t(list, expected);
        }
        {
            Integer[] a = {9, 9};
            Integer[] b = {9, 0, 9};
            Integer[] expected = {8, 9, 9, 9, 1};
            Integer[] list = addLongInteger(a, b);
            t(list, expected);
        }
        end();
    }

    public static void test_multiLongInt() {
        beg();
        {
            int[] a = {0};
            int[] b = {0};
            int[] expected = {0};
            int[] list = multiLongInt(a, b);
            t(list, expected);
        }
        {
            int[] a = {1};
            int[] b = {1};
            int[] expected = {1};
            int[] list = multiLongInt(a, b);
            p(list);
            t(list, expected);
        }
        {
            int[] a = {9};
            int[] b = {9};
            int[] expected = {8, 1};
            int[] list = multiLongInt(a, b);
            p(list);
            t(list, expected);
        }
        {
            int[] a = {9, 0, 9};
            int[] b = {9, 0, 3, 0};
            int[] expected = {8, 2, 0, 8, 2, 7, 0};
            int[] list = multiLongInt(a, b);
            p(list);
            t(list, expected);
        }
        {
            int[] a = {9, 0};
            int[] b = {9, 0};
            int[] expected = {8, 1, 0, 0};
            int[] list = multiLongInt(a, b);
            p(list);
            t(list, expected);
        }
        end();
    }

    public static int[] oneArray() {
        int[] arr = new int[1];
        arr[0] = 1;
        return arr;
    }

    public static void test_power() {
        beg();
        {
            int[] a = {9};
            int n = 0;
            int[] expected = {1};
            int[] list = power(a, n);
            t(list, expected);
        }
        {
            int[] a = {9};
            int n = 1;
            int[] expected = {9};
            int[] list = power(a, n);
            t(list, expected);
        }
        {
            int[] a = {9, 0};
            int n = 1;
            int[] expected = {9, 0};
            int[] list = power(a, n);
            t(list, expected);
        }
        {
            int[] a = {9};
            int n = 2;
            int[] expected = {8, 1};
            int[] list = power(a, n);
            t(list, expected);
        }
        {
            int[] a = {9, 0};
            int n = 2;
            int[] expected = {8, 1, 0, 0};
            int[] list = power(a, n);
            t(list, expected);
        }
        {
            int[] a = {9, 0, 9};
            int n = 3;
            int[] expected = {7, 5, 1, 0, 8, 9, 4, 2, 9};
            int[] list = power(a, n);
            t(list, expected);
        }
    }

    /**
     * power of long integer,  0<= n
     *
     * @param arr
     * @param n
     * @return
     */
    public static int[] power(int[] arr, int n) {
        if (n == 0) {
            return oneArray();
        } else {
            if (n % 2 == 0) {
                return multiLongInt(power(arr, n / 2), power(arr, n / 2));
            } else {
                return multiLongInt(arr, multiLongInt(power(arr, n / 2), power(arr, n / 2)));
            }
        }
    }

    public static void tets_perfectSubstring() {
        beg();
        {
            String s = "aba";
            int k = 2;
            List<String> list = perfectSubstring(s, k);

            List<String> ls1 = Arrays.asList("ab", "ba", "aba");
            List<String> expList = sortListString(ls1);
            List<String> sortedList = sortListString(list);
            t(sortedList, expList);
        }
        {
            String s = "aa";
            int k = 2;
            List<String> list = perfectSubstring(s, k);

            List<String> ls1 = Arrays.asList();
            List<String> expList = sortListString(ls1);
            List<String> sortedList = sortListString(list);
            t(sortedList, expList);
        }
        {
            String s = "ab";
            int k = 2;
            List<String> list = perfectSubstring(s, k);

            List<String> ls1 = Arrays.asList("ab");
            List<String> expList = sortListString(ls1);
            List<String> sortedList = sortListString(list);
            t(sortedList, expList);
        }
        {
            String s = "abc";
            int k = 1;
            List<String> list = perfectSubstring(s, k);

            List<String> ls1 = Arrays.asList("a", "b", "c");
            List<String> expList = sortListString(ls1);
            List<String> sortedList = sortListString(list);
            t(sortedList, expList);
        }
        {
            String s = "aba";
            int k = 1;
            List<String> list = perfectSubstring(s, k);

            List<String> ls1 = Arrays.asList("a", "b", "a");
            List<String> expList = sortListString(ls1);
            List<String> sortedList = sortListString(list);
            t(sortedList, expList);
        }
        {
            String s = "abccb";
            int k = 2;
            List<String> list = perfectSubstring(s, k);

            List<String> ls1 = Arrays.asList("ab", "bc", "ccb", "cb", "bcc", "bccb");
            List<String> expList = sortListString(ls1);
            List<String> sortedList = sortListString(list);
            t(sortedList, expList);
        }
        {
            String s = "";
            int k = 2;
            List<String> list = perfectSubstring(s, k);

            List<String> ls1 = Arrays.asList();
            List<String> expList = sortListString(ls1);
            List<String> sortedList = sortListString(list);
            t(sortedList, expList);
        }
        end();

    }

    public static void test_splitAtRecur() {
        int prevInx = 0;
        int currInx = 0;
        String s = "List<ab> ls";
        Set<String> set = new HashSet<>(Arrays.asList("<", ">"));
        List<Tuple> ls = splitAtRecur(prevInx, currInx, s, set);
        fl("---------------");
        pl(ls);
    }

    public static void test_splitStrWhenChar() {
        beg();
        {
            String s = "<e>";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            List<String> list = splitStrWhenChar(s, set);
            List<String> expList = Arrays.asList("<", "e", ">");
            t(list, expList);
        }
        end();
    }

    public static void test_replaceSetOfString() {
        beg();
        {
            String s = "";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            String str = replaceSetOfString(s, set, "-");
            String expStr = "";
            t(str, expStr);
        }
        {
            String s = "<e>";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            String str = replaceSetOfString(s, set, "");
            String expStr = "e";
            t(str, expStr);
        }
        {
            String s = "<>";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            String str = replaceSetOfString(s, set, "");
            String expStr = "";
            t(str, expStr);
        }
        {
            String s = "<<<eb>>>";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            String str = replaceSetOfString(s, set, "");
            String expStr = "eb";
            t(str, expStr);
        }
        {
            String s = "<<<eb<abc>>>";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            String str = replaceSetOfString(s, set, "");
            String expStr = "ebabc";
            t(str, expStr);
        }
        {
            String s = "<eb<abc>";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            String str = replaceSetOfString(s, set, "-");
            String expStr = "-eb-abc-";
            t(str, expStr);
        }
        {
            String s = "ab<c>de";
            Set<Character> set = new HashSet<>(Arrays.asList('<', '>'));
            String str = replaceSetOfString(s, set, "-");
            String expStr = "ab-c-de";
            t(str, expStr);
        }
        end();
    }

    public static void test_isPowerOf2() {
        beg();
        {
            {
                Boolean b = isPowerOf2(0);
                t(b, false);
            }
            {
                Boolean b = isPowerOf2(1);
                t(b, true);
            }
            {
                Boolean b = isPowerOf2(2);
                t(b, true);
            }
            {
                Boolean b = isPowerOf2(8);
                t(b, true);
            }
            {
                Boolean b = isPowerOf2(9);
                t(b, false);
            }
        }
    }

    public static void test_isSubstring() {
        beg();
        {
            String s1 = "";
            String s2 = "";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "a";
            String s2 = "a";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "a";
            String s2 = "ab";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "aba";
            String s2 = "ab";
            boolean b = isSubstring(s1, s2);
            t(b, false);
        }
        {
            String s1 = "b";
            String s2 = "abb";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "b";
            String s2 = "bb";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "bb";
            String s2 = "bb";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "bb";
            String s2 = "bba";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "bb";
            String s2 = "cbba";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "bbc";
            String s2 = "cbba";
            boolean b = isSubstring(s1, s2);
            t(b, false);
        }
        {
            String s1 = "abbc";
            String s2 = "kkkabbkabccabbc";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        {
            String s1 = "abbc";
            String s2 = "kkkabbkabccabbck";
            boolean b = isSubstring(s1, s2);
            t(b, true);
        }
        end();
    }

    static void test_containsSubstr() {
        beg();
        {
            String s1 = "";
            String s2 = "abc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "a";
            String s2 = "abc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "b";
            String s2 = "abc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "c";
            String s2 = "abc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "ab";
            String s2 = "abc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "bc";
            String s2 = "abc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "abc";
            String s2 = "abc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "kbc";
            String s2 = "abckbeekbc";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        {
            String s1 = "kbc";
            String s2 = "abckbeekbcff";
            boolean b = containsSubstr(s2, s1);
            t(b, true);
        }
        end();
    }


    /**
     * from Heap.java
     */
    public static void test_heapSort() {
        beg();
        {
            fl("heapSort() 1");
            List<Integer> ls = list();
            List<Integer> expList = list();
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }
        {
            fl("heapSort() 2");
            List<Integer> ls = list(2);
            List<Integer> expList = list(2);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }
        {
            fl("heapSort() 3");
            List<Integer> ls = list(2, 1);
            List<Integer> expList = list(1, 2);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }
        {
            fl("heapSort() 4");
            List<Integer> ls = list(2, 1, 4, 1, 2);
            List<Integer> expList = list(1, 1, 2, 2, 4);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }
        {
            fl("heapSort() 5");
            List<Integer> ls = list(2, 1, 4, 1, 2, 0);
            List<Integer> expList = list(0, 1, 1, 2, 2, 4);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }
        end();
    }


    public static void test_substringIndex() {
        {
            fl("ab");
            String s1 = "";
            String s2 = "ab";
            int index = substringIndex(s2, s1);
            t(index, -1);
        }
        {
            fl("2");
            String s1 = "";
            String s2 = "";
            int index = substringIndex(s2, s1);
            t(index, -1);
        }
        {
            fl("a");
            String s1 = "a";
            String s2 = "a";
            int index = substringIndex(s2, s1);
            t(index, 0);
        }
        {
            fl("a ab");
            String s1 = "a";
            String s2 = "ab";
            int index = substringIndex(s2, s1);
            t(index, 0);
        }
        {
            fl("a ba");
            String s1 = "a";
            String s2 = "ba";
            int index = substringIndex(s2, s1);
            t(index, 1);
        }
        {
            fl("aa ba");
            String s1 = "aa";
            String s2 = "ba";
            int index = substringIndex(s2, s1);
            t(index, -1);
        }
        {
            fl("aa baa");
            String s1 = "aa";
            String s2 = "baa";
            int index = substringIndex(s2, s1);
            t(index, 1);
        }
        {
            fl("aa aac");
            String s1 = "aa";
            String s2 = "aac";
            int index = substringIndex(s2, s1);
            t(index, 0);
        }
        {
            fl("aa aaa");
            String s1 = "aa";
            String s2 = "aaa";
            int index = substringIndex(s2, s1);
            t(index, 0);
        }
        {
            fl("aa baaa");
            String s1 = "aa";
            String s2 = "baaa";
            int index = substringIndex(s2, s1);
            t(index, 1);
        }
        {
            fl("aac baaac");
            String s1 = "aac";
            String s2 = "baaac";
            int index = substringIndex(s2, s1);
            t(index, 2);
        }
        end();
    }

    public static void test_createBinNoImage() {
        beg();
        {
            Node r = geneBinNoImage();
            printTree(r);
        }
        {
            List<Integer> ls = list(2, 1, 4, 1, 2);
            BST bst = createBinNoImage(ls);
            printTree(bst.root);
            List<Integer> expList = inorderToList(bst.root);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }

        {
            List<Integer> ls = list(9, 1, 4, 12, 3, 8, 11);
            BST bst = createBinNoImage(ls);
            printTree(bst.root);
            List<Integer> expList = inorderToList(bst.root);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            t(expList, sortedList);
        }
        {
            List<Integer> ls = list(9, 1, 4, 12, 3, 8, 11, 20, 4, 7, 3, 23, 2, 6, 9);
            BST bst = createBinNoImage(ls);
            printTree(bst.root);
            List<Integer> expList = inorderToList(bst.root);
            List<Integer> sortedList = heapSort(ls, (x, y) -> x <= y);
            // t(expList, sortedList);
            t(expList, sortedList);
        }
        end();
    }

    public static void test_Heap() {
        beg();
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            Integer size = 1;
            t(heap.size(), size);
            Integer top = (Integer) heap.removeTop();
            t(top, 1);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(2);
            Integer size = 2;
            t(heap.size(), size);
            Integer top = (Integer) heap.removeTop();
            t(top, 1);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(0);
            Integer size = 2;
            t(heap.size(), size);
            Integer top = (Integer) heap.removeTop();
            t(top, 0);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(3);
            heap.insert(0);
            Integer size = 3;
            t(heap.size(), size);
            Integer top = (Integer) heap.removeTop();
            t(top, 0);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(0);
            heap.insert(3);
            Integer size = 3;
            t(heap.size(), size);
            Integer top = (Integer) heap.removeTop();
            t(top, 0);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(0);
            heap.insert(3);
            heap.insert(2);
            Integer size = 4;
            t(heap.size(), size);
            Integer top = (Integer) heap.removeTop();
            t(top, 0);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(0);
            heap.insert(3);
            heap.insert(-1);
            Integer size = 4;
            t(heap.size(), size);
            Integer top = (Integer) heap.removeTop();
            t(top, -1);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(0);
            heap.insert(3);
            heap.insert(-1);
            Integer size = 4;
            t(heap.size(), size);
            heap.removeTop();
            Integer top = (Integer) heap.removeTop();
            t(top, 0);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(0);
            heap.insert(3);
            heap.insert(-1);
            Integer size = 4;
            t(heap.size(), size);
            heap.removeTop();
            heap.removeTop();
            Integer top = (Integer) heap.removeTop();
            t(top, 1);
        }
        {
            fl("test_heap removeTop");
            BiFunction<Integer, Integer, Boolean> f = (x, y) -> x < y;
            Heap heap = new Heap(f);
            heap.insert(1);
            heap.insert(0);
            heap.insert(3);
            heap.insert(-1);
            Integer size = 4;
            t(heap.size(), size);
            heap.removeTop();
            heap.removeTop();
            heap.removeTop();
            Integer top = (Integer) heap.removeTop();
            t(heap.size(), 0);
            t(top, 3);
        }
        {
            {
                fl("test_inorderToList");
                Node node = geneBinNoImage(list(1));
                List<Integer> ls = inorderToList(node);
                t(ls, list(1));
            }
            {
                fl("test_inorderToList");
                Node node = geneBinNoImage(list(4, 9, 1));
                List<Integer> ls = inorderToList(node);
                t(ls, list(1, 4, 9));

            }
            {
                fl("test_inorderToList");
                Node node = geneBinNoImage(list(4, 9, 1, 3, 3, 1));
                List<Integer> ls = inorderToList(node);
                t(ls, list(1, 1, 3, 3, 4, 9));

            }

            end();
        }

    }
    public static void test_deleteMaxNode(){
        beg();
        {
            Node node = geneBinNoImage(list(2, 4, 3, 6));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 3, 4));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(2, 4));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(2, 1));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(1));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9, 6, 5, 7));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 4, 5, 6, 7, 8, 9));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9, 6, 5, 7, 11));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 4, 5, 6, 7, 8, 9, 10));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 4, 8, 9));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9, 1));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(1, 2, 4, 8, 9));
        }
        end();
    }
    public static void test_deleteMinNode(){
        beg();

        {
            Node node = geneBinNoImage(list(2, 4));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(4));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(2, 4, 6));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(4, 6));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(4, 1, 2));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 4));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(4, 1, 2, 3));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 3, 4));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(6, 1));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(6));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(8, 2, 5, 4, 6));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(4, 5, 6, 8));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 6, 4, 8, 3, 5));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(3, 4, 5, 6, 8, 10));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9, 6, 5, 7));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(4, 5, 6, 7, 8, 9, 10));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(2, 4, 3, 6));
            Node parent = null;
            deleteMinNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(3, 4, 6));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(2, 4, 3, 6));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 3, 4));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(2, 4));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(2, 1));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(1));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9, 6, 5, 7));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 4, 5, 6, 7, 8, 9));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9, 6, 5, 7, 11));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 4, 5, 6, 7, 8, 9, 10));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(2, 4, 8, 9));
        }
        Ut.l();
        {
            Node node = geneBinNoImage(list(10, 2, 8, 4, 9, 1));
            Node parent = null;
            deleteMaxNode(node);
            List<Integer> ls = inorderToList(node);
            printTree(node);
            t(ls, list(1, 2, 4, 8, 9));
        }
        end();
    }

    /**
     8
     4        9
     1     6

     8
     9        4
     6     1
     */
    public static void test_flipBinTree(){
        beg();
        {
            {
                Node node = null;
                Node flipNode = flipBinTree(node);
                printTree(flipNode);
                List<Integer> ls = inorderToList(flipNode);
                t(ls, list());
            }
            {
                Node node = geneBinNoImage(list(8));
                Node flipNode = flipBinTree(node);
                printTree(flipNode);
                List<Integer> ls = inorderToList(flipNode);
                t(ls, list(8));
            }
            {
                Node node = geneBinNoImage(list(8, 1));
                Node flipNode = flipBinTree(node);
                printTree(flipNode);
                List<Integer> ls = inorderToList(flipNode);
                t(ls, list(8, 1));
            }
            {
                Node node = geneBinNoImage(list(1, 2, 3));
                Node flipNode = flipBinTree(node);
                printTree(flipNode);
                List<Integer> ls = inorderToList(flipNode);
                t(ls, list(3, 2, 1));
            }
            {
                Node node = geneBinNoImage(list(8, 1, 9));
                Node flipNode = flipBinTree(node);
                printTree(flipNode);
                List<Integer> ls = inorderToList(flipNode);
                t(ls, list(9, 8, 1));
            }
            {
                Node node = geneBinNoImage(list(8, 4, 1, 6, 9));
                Node flipNode = flipBinTree(node);
                printTree(flipNode);
                List<Integer> ls = inorderToList(flipNode);
                t(ls, list(9, 8, 6, 4, 1));
            }
        }
        end();
    }
    public static void test_flipBinTreeMutable(){
        beg();
        {
            {
                Node node = null;
                flipBinTreeMutable(node);
                printTree(node);
                List<Integer> ls = inorderToList(node);
                t(ls, list());
            }
            {
                Node node = geneBinNoImage(list(8));
                flipBinTreeMutable(node);
                printTree(node);
                List<Integer> ls = inorderToList(node);
                t(ls, list(8));
            }
            {
                Node node = geneBinNoImage(list(8, 1));
                flipBinTreeMutable(node);
                printTree(node);
                List<Integer> ls = inorderToList(node);
                t(ls, list(8, 1));
            }
            {
                Node node = geneBinNoImage(list(1, 2, 3));
                flipBinTreeMutable(node);
                printTree(node);
                List<Integer> ls = inorderToList(node);
                t(ls, list(3, 2, 1));
            }
            {
                Node node = geneBinNoImage(list(8, 1, 9));
                flipBinTreeMutable(node);
                printTree(node);
                List<Integer> ls = inorderToList(node);
                t(ls, list(9, 8, 1));
            }
            {
                Node node = geneBinNoImage(list(8, 4, 1, 6, 9));
                flipBinTreeMutable(node);
                printTree(node);
                List<Integer> ls = inorderToList(node);
                t(ls, list(9, 8, 6, 4, 1));
            }
        }
        end();
    }
    public static void test_isBSTRecurve(){
	beg();
	{
	    {
		Node root = null;
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = new Node(9);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4, 12), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(9, 4, 1, 10, 20, 8), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = geneBinNoImage(list(2, 6, 3, 40, 26, 9, 4, 1, 10, 20, 8), false);
		Node[] arr = new Node[1];
		Boolean b = isBSTRecurve(root, arr);
		t(b, true);
	    }
	    {
		Node root = node(9);
		Node[] arr = new Node[1];
		root.right = node(2);
		Boolean b = isBSTRecurve(root, arr);
		t(b, false);
	    }
	    {
		/**
		          9
                      20
		 */
		Node root = node(9);
		Node[] arr = new Node[1];
		root.left = node(20);
		Boolean b = isBSTRecurve(root, arr);
		t(b, false);
	    }
	    {
		/**
		             9
			 20      30
		     
		 */
		
		Node root = node(9);
		root.left = node(20);
		Node[] arr = new Node[1];
		root.right = node(30);
		Boolean b = isBSTRecurve(root, arr);
		t(b, false);
		
	    }
	    {
		
		Node root = new Node(9);
		Node[] arr = new Node[1];
		root.left = new Node(20);
		root.left.left = new Node(1);
		Boolean b = isBSTRecurve(root, arr);
		t(b, false);
	    }
	}
    }
    public static void test1_isBSTRecurve(){
        beg();
	{
	    Node node = geneBinNoImage(list(9), false);
	    Node prev = null;
	    Node[] arr = new Node[1];
	    Boolean b = isBSTRecurve(node, arr);
	    t(b, true);
	}
	{
	    Node node = geneBinNoImage(list(9, 2), false);
	    Node[] arr = new Node[1];
	    Boolean b = isBSTRecurve(node, arr);
	    t(b, true);
	}
	{
	    Node node = geneBinNoImage(list(9, 2, 10), false);
	    Node[] arr = new Node[1];
	    Boolean b = isBSTRecurve(node, arr);
	    t(b, true);
	}
	{
	    Node node = geneBinNoImage(list(), false);
	    Node[] arr = new Node[1];
	    Boolean b = isBSTRecurve(node, arr);
	    t(b, true);
	}
	{
	    Node node = geneBinNoImage(list(9, 4, 10, 20, 8, 3, 1), false);
	    Node[] arr = new Node[1];
	    Boolean b = isBSTRecurve(node, arr);
	    t(b, true);
	}
        end();
    }

    public static void test_MaxStack(){
        beg();
	{
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(1);
		mstack.insert(2);
		Optional opt = mstack.getMax();
		t(3, opt.get());
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		Optional opt = mstack.getMax();
		t(3, opt.get());
	    }
	    {
		MaxStack mstack = new MaxStack();
		Optional opt = mstack.getMax();
		t(opt.isPresent(), false);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		Optional opt = mstack.getMax();
		t(opt.isPresent(), true);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(4);
		Optional opt = mstack.getMax();
		t(opt.get(), 4);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(4);
		mstack.insert(1);
		mstack.pop();
		Optional opt = mstack.getMax();
		t(opt.get(), 4);
	    }
	    {
		MaxStack mstack = new MaxStack();
		mstack.insert(3);
		mstack.insert(4);
		mstack.pop();
		Optional opt = mstack.getMax();
		t(opt.get(), 3);
	    }
	}
        end();
    }
    
    public static void test_fillArrLeft(){
        beg();
	{
	    {
		fl("fillArrLeft 1");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrLeft(arr, 1, 2);
		Integer[] expArr = listToArrayInteger(list(2, 1));
		t(expArr, farr);
	    }
	    {
		fl("fillArrLeft 2");
		Integer[] arr = listToArrayInteger(list(1, 2, 3));
		Integer[] farr = fillArrLeft(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(2, 2, 1, 2, 3));
		t(expArr, farr);
	    }
	    {
		fl("fillArrLeft 3");
		Integer[] arr = listToArrayInteger(list());
		Integer[] farr = fillArrLeft(arr, 1, 9);
		Integer[] expArr = listToArrayInteger(list(9));
		t(expArr, farr);
	    }
	    {
		fl("fillArrLeft 4");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrLeft(arr, 1, 9);
		Integer[] expArr = listToArrayInteger(list(9, 1));
		t(expArr, farr);
	    }
	}
        end();
    }
    
    public static void test_fillArrRight(){
        beg();
	{
	    {
		fl("fillArrRight 1");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrRight(arr, 1, 2);
		Integer[] expArr = listToArrayInteger(list(1, 2));
		t(expArr, farr);
	    }
	    {
		fl("fillArrRight 2");
		Integer[] arr = listToArrayInteger(list(1, 2, 3));
		Integer[] farr = fillArrRight(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(1, 2, 3, 2, 2));
		t(expArr, farr);
	    }
	    {
		fl("fillArrRight 3");
		Integer[] arr = listToArrayInteger(list());
		Integer[] farr = fillArrRight(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(2, 2));
		t(expArr, farr);
	    }
	    {
		fl("fillArrRight 4");
		Integer[] arr = listToArrayInteger(list(1));
		Integer[] farr = fillArrRight(arr, 2, 2);
		Integer[] expArr = listToArrayInteger(list(1, 2, 2));
		t(expArr, farr);
	    }
	}
        end();
    }
    public static void test_addLongIntegerNew(){
        beg();
	{
	    {
		Integer[] arr1 = array(9, 9, 9);
		Integer[] arr2 = array(9, 9);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(1, 0, 9, 8));
	    }
	    {
		Integer[] arr1 = array(0);
		Integer[] arr2 = array(0);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(0, 0));
	    }
	    {
		Integer[] arr1 = array(1);
		Integer[] arr2 = array(0);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(0, 1));
	    }
	    {
		Integer[] arr1 = array(9);
		Integer[] arr2 = array(9);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(1, 8));
	    }
	    {
		Integer[] arr1 = array(9, 9);
		Integer[] arr2 = array(9, 9);
		Integer[] arr = addLongIntegerNew(arr1, arr2);
		t(arr, array(1, 9, 8));
	    }
	}
        end();
    }

  
    public static void test_multiLongInteger(){
	beg();
	{
          /*
           // 27-08-2020
           // TODO: fix error
	    {
		var s1 = list(0);
		var s2 = list(1);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(0, 0));
		t(listToNumBase(s3, 10), 0);
	    }
          */
            /*
	    {
		var s1 = list(1);
		var s2 = list(1);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(0, 1));
		t(listToNumBase(s3, 10), 1);
	    }
	    {
		var s1 = list(9);
		var s2 = list(1);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(0, 9));
		t(listToNumBase(s3, 10), 9);
	    }
            */
            /*
	    {
		var s1 = list(9);
		var s2 = list(9);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(8, 1));
		t(listToNumBase(s3, 10), 81);
	    }
	    {
		var s1 = list(9);
		var s2 = list(1, 9);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(1, 7, 1));
		t(listToNumBase(s3, 10), 171);
	    }
	    {
		var s1 = list(0);
		var s2 = list(1, 9);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(0, 0, 0));
		t(listToNumBase(s3, 10), 0);
	    }
	    {
		var s1 = list(0);
		var s2 = list(1, 0, 9);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(0, 0, 0, 0));
		t(listToNumBase(s3, 10), 0);
	    }
	    {
		var s1 = list(9);
		var s2 = list(1, 0, 9);
		var s3 = multiLongInteger(s1, s2);
		t(s3, list(0, 9, 8, 1));
		t(listToNumBase(s3, 10), 981);
	    }
	    {
		var s1 = list(9, 9);
		var s2 = list(9);
		var s3 = multiLongInteger(s1, s2);
		t(listToNumBase(s3, 10), 891);
	    }
            */

            /*
	    {

		var s1 = list(7, 9);
		var s2 = list(6);
		var s3 = multiLongInteger(s1, s2);
		t(listToNumBase(s3, 10), 474);
	    }

            
	    {

		var s1 = list(7, 9);
		var s2 = list(6, 1);
		var s3 = multiLongInteger(s1, s2);
		t(listToNumBase(s3, 10), 4819);
		p(s3);
	    }
            
            
	    {
		var s1 = list(7, 9);
		var s2 = list(6, 1, 8);
		var s3 = multiLongInteger(s1, s2);
		t(listToNumBase(s3, 10), 48822);
		p(s3);
	    }
            */
            
	}
	end();
    }
    public static void test_removeAdjacentDuplicateChar(){
	beg();
	{
	    {
		String s = removeAdjacentDuplicateChar("a", 'b');
		t(s, "a");
	    }
	    {
		String s = removeAdjacentDuplicateChar("bb", 'b');
		t(s, "b");
	    }
	    {
		String s = removeAdjacentDuplicateChar("bbb", 'b');
		t(s, "b");
	    }
	    {
		String s = removeAdjacentDuplicateChar("abbb", 'b');
		t(s, "ab");
	    }
	    {
		String s = removeAdjacentDuplicateChar("/a/b//", '/');
		t(s, "/a/b/");
	    }
	    {
		String s = removeAdjacentDuplicateChar("//a////b//", '/');
		t(s, "/a/b/");
	    }
	    {
		String s = removeAdjacentDuplicateChar("a////b", '/');
		t(s, "a/b");
	    }
	    {
		String s = removeAdjacentDuplicateChar("//", '/');
		t(s, "/");
	    }
	    {
		String s = removeAdjacentDuplicateChar("/", '/');
		t(s, "/");
	    }
	}
	end();
    }
    public static void test_firstLastPosition(){
        beg();
	{
	    Integer[] arr = {1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 0));
	}
	{
	    Integer[] arr = {1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 0));
	}
	{
	    Integer[] arr = {0, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 1));
	}
	{
	    Integer[] arr = {0, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 1));
	}
	{
	    Integer[] arr = {1, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 1));
	}
	{
	    Integer[] arr = {1, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 1));
	}
	{
	    Integer[] arr = {0, 1, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 2));
	}
	{
	    Integer[] arr = {0, 1, 1, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 3));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 3));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 2, 2, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 3));
	}
	{
	    Integer[] arr = {0, 1, 1, 1, 1, 2, 2, 2};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(1, 4));
	}
	{
	    Integer[] arr = {0, 1, 1, 1, 1, 2, 2, 2};
	    Integer n = 2;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(5, 7));
	}
	{
	    Integer[] arr = {0, 1, 1, 1, 1, 2, 2, 2, 3};
	    Integer n = 2;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(5, 7));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 1, 1};
	    Integer n = 1;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list(0, 5));
	}
	{
	    Integer[] arr = {1, 1, 1, 1, 1, 1};
	    Integer n = 2;
	    Integer lo = 0;
	    Integer hi = len(arr) - 1;
	    var ret = firstLastPostion(arr, n, lo, hi);
	    t(ret, list());
	}


        end();
    }

  public static void test_swap_arr(){
    beg();
    {
      Integer[] arr = {1, 2, 3};
      Integer[] expArr = {2, 1, 3};
      swap(arr, 0, 1);
      t(arr, expArr);
    }
    {
      Integer[] arr = {1, 2};
      Integer[] expArr = {2, 1};
      swap(arr, 0, 1);
      t(arr, expArr);
    }
    {
      Integer[] arr = {1, 2, 3};
      Integer[] expArr = {1, 2, 3};
      swap(arr, 1, 1);
      t(arr, expArr);
    }
    end();
  }
  
  public static void test_swapCol(){
    beg();
    {
      Integer[][] arr = {
        { 1,   0,   0,  1},
        { 1,   1,   0,  1},
        { 0,   1,   0,  1},
        { 0,   1,   1,  0}};
      
      Integer[][] expArr = {
        {0,  1, 0,  1},
        {1,  1, 0,  1},
        {1,  0, 0,  1},
        {1,  0, 1,  0}};
      
      swapCol(arr, 0, 1);
      t(arr, expArr);
    }
    end();
  }
  
  public static void test_swapRow(){
    beg();
    {
      Integer[][] arr = {
        { 1,   0,   0,  1},
        { 1,   1,   0,  1},
        { 0,   1,   0,  1},
        { 0,   1,   1,  0}};
      
      Integer[][] expArr = {
        { 1,   1,   0,  1},
        { 1,   0,   0,  1},
        { 0,   1,   0,  1},
        { 0,   1,   1,  0}};
      
      swapRow(arr, 0, 1);
      t(arr, expArr);
    }
    end();
  }
  public static void test_partition(){              
    beg();                                          
   {                                               
      Integer[] arr = {4};                          
      Integer[] expArr = {4};                       
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 0);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      Integer[] arr = {4, 0};                       
      Integer[] expArr = {0, 4};                    
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 0);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      Integer[] arr = {4, 0, 1};                    
      Integer[] expArr = {0, 1, 4};                 
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 1);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      Integer[] arr = {0, 1};                       
      Integer[] expArr = {0, 1};                    
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 1);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      Integer[] arr = {0, 1, 2};                    
      Integer[] expArr = {0, 1, 2};                 
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 2);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      Integer[] arr = {0, 1, 3, 2};                 
      Integer[] expArr = {0, 1, 2, 3};              
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 2);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      Integer[] arr = {3, 2, 1};                    
      Integer[] expArr = {1, 2, 3};                 
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 0);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      Integer[] arr = {3, 2, 1, 3};                 
      Integer[] expArr = {3, 2, 1, 3};              
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 3);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      // arr[i] <= pivot                            
      Integer[] arr = {3, 3};                       
      Integer[] expArr = {3, 3};                    
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 1);                                  
      t(arr, expArr);                               
    }                                               
    {                                               
      // arr[i] <= pivot                            
      Integer[] arr = {3, 2, 3};                    
      Integer[] expArr = {3, 2, 3};                 
      Integer lo = 0;                               
      Integer hi = len(arr) - 1;                    
      Integer pivot = partition(arr, lo, hi);       
      t(pivot, 2);                                  
      t(arr, expArr);                               
    }                                               
    end();                                          
  }

  public static void test_fillListStr2d(){
    beg();
    {
      fl("fillListStr2d");
      var ss = list(list("a", "b"), list("a", "b", "c", "e"));
      var ls = fillListStr2d(ss);
      var expls = list(list("a", "b", "", ""), list("a", "b", "c", "e"));
      t(ls, expls);
    }
    {
      fl("fillListStr2d");
      var ss = list(list("a", "b", "c"), list("a"));
      var ls = fillListStr2d(ss);
      var expls = list(list("a", "b", "c"), list("a", "", ""));
      t(ls, expls);
    }
    {
      {
        fl("removeIndexCol");
        var ss = list(list("a"), list("a"));
        var lss = removeIndexCol(0, ss);
        var expls = list(list(), list());
        t(lss, expls);
        pp(lss);
      }
      {
        fl("removeIndexCol");
        var ss = list(list("a", "b", "k"), list("a", "b", "c"));
        var lss = removeIndexCol(1, ss);
        var expls = list(list("a", "k"), list("a", "c"));
        t(lss, expls);
        pp(lss);
      }
      {
        fl("removeIndexCol");
        var ss = list(list("a", "b", "k"), list("a", "b", "c"));
        var lss = removeIndexCol(0, ss);
        var expls = list(list("b", "k"), list("b", "c"));
        t(lss, expls);
        pp(lss);
      }
    }      
    end();
  }
  public static void test_insertIndexToRight(){
    beg();
    {
      {
        List<String> ls = list();
        var myls = insertIndexToRight(0, "a", ls);
        var expls = list("a");
        t(myls, expls);
      }
      {
        List<String> ls = list("a", "b");
        var myls = insertIndexToRight(1, "c", ls);
        var expls = list("a", "c", "b");
        t(myls, expls);
      }
      {
        List<String> ls = list("a", "b");
        var myls = insertIndexToRight(0, "c", ls);
        var expls = list("c", "a", "b");
        t(myls, expls);
      }
      {
        List<String> ls = list("a", "b", "c");
        List<String> myls = insertIndexToRight(1, "1", ls);
        List<String> expls = list("a", "1", "b", "c");
        t(myls, expls);
        Print.pp(ls);
      }
    }
    end();
  }
  
  public static void test_replaceIndexCol(){
    beg();
    {
      {
        var ss = list(list("a", "b", "k"), list("a", "b", "c"));
        var rr = list("1", "2");
        var expss = list(list("1", "b", "k"), list("2", "b", "c"));
        var nss = replaceIndexCol(0, rr, ss);
        fl("nss");
        t(nss, expss);
      }
      {
        var ss = list(list("a", "b", "k"), list("a", "b", "c"));
        var rr = list("1", "2");
        var expss = list(list("a", "1", "k"), list("a", "2", "c"));
        var nss = replaceIndexCol(1, rr, ss);
        fl("nss");
        t(nss, expss);
      }
      {
        var ss = list(list("a", "b", "k"), list("a", "b", "c"));
        var rr = list("1", "2");
        var expss = list(list("a", "b", "1"), list("a", "b", "2"));
        var nss = replaceIndexCol(2, rr, ss);
        fl("nss");
        t(nss, expss);
      }
    }
    end();
  }
  public static void test_splitWhileStr(){
    beg();
    {
      {
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', "ab");
        t(t, tuple("", "ab"));
      }
      {
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', " ab");
        t(t, tuple(" ", "ab"));
      }
      {
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', " ab ");
        t(t, tuple(" ", "ab "));
      }
      {
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', "ab ");
        t(t, tuple("", "ab "));
      }
      {
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', " ");
        t(t, tuple(" ", ""));
      }
      {
        Tuple<String, String> t = splitWhileStr(x -> x == ' ', "  ");
        t(t, tuple("  ", ""));
      }
    }
    end();
  }
  
  public static void test_splitIf(){
    beg(); 
    {
      {
        fl("test_splitIf 0");
        Triple<List<String>, List<String>,List<String>> ret = splitIf(x -> x.trim().equals("="), list("a", "=", "c"));
        t(ret, triple(ll("a"), ll("="), ll("c")));
        pp(ret);
      }
      {
        fl("test_splitIf 1");
        Triple<List<String>, List<String>,List<String>> ret = splitIf(x -> x.trim().equals("="), list("a", "b"));
        t(ret, triple(ll(), ll(), ll("a", "b")));
        pp(ret);
      }
      {
        fl("test_splitIf 2");
        Triple<List<String>, List<String>,List<String>> ret = splitIf(x -> x.trim().equals("="), list("=", "="));
        t(ret, triple(ll(), ll("="), ll("=")));
        pp(ret);
      }
      {
        fl("test_splitIf 3");
        Triple<List<String>, List<String>,List<String>> ret = splitIf(x -> x.trim().equals("="), list("=", "=", "="));
        t(ret, triple(ll(), ll("="), ll("=", "=")));
        pp(ret);
      }
      {
        fl("test_splitIf 4");
        Triple<List<String>, List<String>,List<String>> ret = splitIf(x -> x.trim().equals("="), list());
        t(ret, triple(ll(), ll(), ll()));
        pp(ret);
      }
      {
        fl("test_splitIf 5");
        Triple<List<String>, List<String>,List<String>> ret = splitIf(x -> x.trim().equals(""), list());
        t(ret, triple(ll(), ll(), ll()));
        pp(ret);
      }
      
    }
    end();
  }
  public static void test_splitNStr(){
    beg(); 
    {
      {
        fl("test_splitNStr 0");
        List<String> s = splitNStr(" ab");
        var expStr = list(" ", "ab");
        t(s, expStr);
      }
      {
        fl("test_splitNStr 1");
        List<String> s = splitNStr(" ab ");
        var expStr = list(" ", "ab", " ");
        t(s, expStr);
      }
      {
        fl("test_splitNStr 2");
        List<String> s = splitNStr("ab");
        var expStr = list("ab");
        t(s, expStr);
      }
      {
        fl("test_splitNStr 3");
        List<String> s = splitNStr(" ");
        var expStr = list(" ");
        t(s, expStr);
      }
      {
        fl("test_splitNStr 4");
        List<String> s = splitNStr("  ");
        var expStr = list("  ");
        t(s, expStr);
      }
      {
        fl("test_splitNStr 5");
        List<String> s = splitNStr(" a b ");
        var expStr = list(" ", "a", " ", "b", " ");
        t(s, expStr);
      }
      {
        fl("test_splitNStr 6");
        List<String> s = splitNStr("  ab  def  ");
        var expStr = list("  ", "ab", "  ", "def", "  ");
        t(s, expStr);
      }
      {
        fl("test_splitNStr 7");
        String str = "  ab  def  ";
        List<String> ls = splitNStr(str);
        var expStr = foldl((x, y) -> x + y, "", ls);
        t(str, expStr);
      }
      {
        fl("test_splitNStr 8");
        String str = " x = 3 + 63 * (199 * 800) ; ";
        List<String> ls = splitNStr(str);
        var expStr = foldl((x, y) -> x + y, "", ls);
        t(str, expStr);
      }
    }
    end();
  }

  public static void test_tableInteger(){
    beg(); 
    {
      {
        List<List<Integer>> lss = tableInteger("1 2; 3 4 ; ");
        List<List<Integer>> expls = ll(ll(1, 2), ll(3, 4));
        fl("lss");
        
        t(lss, expls);
        pp(lss);
      }
      {
        List<List<Integer>> lss = tableInteger("1; 2; ");
        List<List<Integer>> expls = ll(ll(1), ll(2));
        t(lss, expls);
        fl("lss");
        pp(lss);
      }
      {
        List<List<Integer>> lss = tableInteger("" + 
                                           "1 1 2 4;" + 
                                           "4 5 6 7;" +
                                           "1 0 3 4;" +
                                          ""
                                          );
        List<List<Integer>> expls = ll(ll(1, 1, 2, 4), ll(4, 5, 6, 7), ll(1, 0, 3, 4));
        t(lss, expls);
        fl("lss");
        pp(lss);
      }
      {
        List<List<Integer>> lss = tableInteger("" + 
                                           "1 1 2 4;" + 
                                           "4 5 6;" +
                                           "1 0 3 4;" +
                                          ""
                                          );
        List<List<Integer>> expls = ll(ll(1, 1, 2, 4), ll(4, 5, 6), ll(1, 0, 3, 4));
        t(lss, expls);
        fl("lss");
        pp(lss);
      }
    }
    end();
  }
  public static void test_foldStrDrop2(){
    beg(); 
    {
      {
        var ls = list("a", "b", "c");
        var lss = foldStrDrop("-", ls);
        var explss = "a-b-c";
        t(lss, explss);
      }
      {
        var ls = list("");
        var lss = foldStrDrop("", ls);
        var explss = "";
        t(lss, explss);
      }
      {
        var ls = list("a");
        var lss = foldStrDrop("", ls);
        var explss = "a";
        t(lss, explss);
      }
      {
        var ls = list(" a", "b ");
        var lss = foldStrDrop("-", ls);
        var explss = " a-b ";
        t(lss, explss);
      }
    }
    end();
  }

  public static void test_foldStrDrop(){
    beg(); 
    {
      {
        var ls = list("a", "b", "c");
        var lss = foldStrDrop(ls);
        var explss = "a b c";
        t(lss, explss);
      }
      {
        var ls = list("");
        var lss = foldStrDrop(ls);
        var explss = "";
        t(lss, explss);
      }
      {
        var ls = list("a");
        var lss = foldStrDrop(ls);
        var explss = "a";
        t(lss, explss);
      }
      {
        var ls = list(" a ", " b ");
        var lss = foldStrDrop(ls);
        var explss = " a   b ";
        t(lss, explss);
      }
    }
    end();
  }
  
  public static void test_splitMatch(){
    {
      beg();
      {
        var triple = splitMatch("ab", "acabe");
        t(triple.x, "ac");
        t(triple.y, "ab");
        t(triple.z, "e");
      }
      {
        var triple = splitMatch("a", "acabe");
        t(triple.x, "");
        t(triple.y, "a");
        t(triple.z, "cabe");
      }
      {
        var triple = splitMatch("a", "acabe");
        t(triple.x, "");
        t(triple.y, "a");
        t(triple.z, "cabe");
      }
      {
        var triple = splitMatch("e", "acabe");
        t(triple.x, "acab");
        t(triple.y, "e");
        t(triple.z, "");
      }
      {
        var triple = splitMatch("e", "e");
        t(triple.x, "");
        t(triple.y, "e");
        t(triple.z, "");
      }
      {
        var triple = splitMatch("ee", "ee");
        t(triple.x, "");
        t(triple.y, "ee");
        t(triple.z, "");
      }
      {
        var triple = splitMatch(":=", "x := y = z");
        t(triple.x, "x ");
        t(triple.y, ":=");
        t(triple.z, " y = z");
      }
      end();
    }
  }
  public static void test_splitRegexOne(){
    beg();
    {
      {
        String str = " abc rst ";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, " ");
        t(triple.y, "abc");
        t(triple.z, " rst ");
      }
      {
        String str = "a";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, "");
        t(triple.y, "a");
        t(triple.z, "");
      }
      {
        String str = " a";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, " ");
        t(triple.y, "a");
        t(triple.z, "");
      }
      {
        String str = " a ";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, " ");
        t(triple.y, "a");
        t(triple.z, " ");
      }
      {
        String str = " a- ";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, " ");
        t(triple.y, "a");
        t(triple.z, "- ");
      }
      {
        String str = " aA ";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, " ");
        t(triple.y, "a");
        t(triple.z, "A ");
      }
      {
        String str = " a( ";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, " ");
        t(triple.y, "a");
        t(triple.z, "( ");
      }
      {
        String str = " = ";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, "");
        t(triple.y, "");
        t(triple.z, "");
      }
      {
        String str = " =a=";
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+)");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, " =");
        t(triple.y, "a");
        t(triple.z, "=");
      }
      {
        String str = "void fun(int n){"; // => fun(
        Triple<String, String, String> triple = splitRegexOne(str, "([a-z]+\\s*\\()");
        pl("str=" + str);
        pl("x=" + triple.x);
        pl("y=" + triple.y);
        pl("z=" + triple.z);
        t(triple.x, "void ");
        t(triple.y, "fun(");
        t(triple.z, "int n){");
      }
    }
    end();
  }
  public static void test_listToArray2d(){
    {
      beg();
      {
        var ls = ll(ll(1, 2, 3), ll(2, 3, 4));
        Integer[][] arr = listToArrayInteger2d(ls);
        Integer[][] expected = {{1, 2, 3},
                                {2, 3, 4}};
        t(arr, expected);
      }
      {
        var ls = ll(ll(1), ll(2));
        Integer[][] arr = listToArrayInteger2d(ls);
        Integer[][] expected = {{1},
                                {2}};
        t(arr, expected);
      }
      {
        var ls = ll(ll(1));
        Integer[][] arr = listToArrayInteger2d(ls);
        Integer[][] expected = {{1}};
        t(arr, expected);
      }
      end();
    }
  }

  public static void test_rotate_list(){
    beg();
    {
      /** 
          1 2
          3 4 
          
          3 1
          4 2
      */
      var ls = ll(ll(1, 2), ll(3, 4));
      var lss = rotate(ls);
      var expected = ll(ll(3, 1), ll(4, 2));
      t(lss, expected);
    }
    end();
  }
  public static void test_transpose_new(){
    beg();
    {
      {
        var ls = ll(ll(), ll());
        var exps = ll(ll(), ll());
        var ls1 = transpose_new(ls);
        t(ls1, exps);
      }
      {
        var ls = ll(ll(1), ll(2));
        var exps = ll(ll(1, 2));
        var ls1 = transpose_new(ls);
        t(ls1, exps);
      }
      {
        /**
           1 2
           3 4
           =>
           1 3
           2 4
         */
        var ls = ll(ll(1, 2), ll(3, 4));
        var exps = ll(ll(1, 3), ll(2, 4));
        var ls1 = transpose_new(ls);
        t(ls1, exps);
      }
      {
        /**
           1 2 3
           4 5 6
           =>
           1 4
           2 5
           3 6
         */
        var ls = ll(ll(1, 2, 3), ll(4, 5, 6));
        var exps = ll(ll(1, 4), ll(2, 5), ll(3, 6));
        var ls1 = transpose_new(ls);
        t(ls1, exps);
      }
    }
    end();
  }
  
  public static void test_flipX(){
    beg();
    {
      {
        var lss = ll(ll(1), ll(2));
        var lsf = flipX(flipX(lss));
        fl("lsf");
        pp(lsf);
        t(lss, lsf);
      }
      {
        var s = ll(ll(), ll());
        var fs = flipX(s);
        t(s, fs);
      }
      {
        /**
           1 2
           3 4
           =>
           3 4
           1 2
         */
        var s = ll(ll(1, 2), ll(3, 4));
        var fs = flipX(s);
        var exps = ll(ll(3, 4), ll(1, 2));
        t(fs, exps);
        fl("fs");
        pp(fs);
      }
    }
    end();
  }
  
  public static void test_flipY(){
    beg();
    {
      {
        var s = ll(ll(), ll());
        var fs = flipY(s);
        t(s, fs);
      }
      {
        /**
           1 2
           3 4
           =>
           2 1
           4 3
        */
        var s = ll(ll(1, 2), ll(3, 4));
        var fs = flipY(s);
        var exps = ll(ll(2, 1), ll(4, 3));
        t(fs, exps);
      }
      {
        /**
           1 2 3
           4 5 6
           =>
           3 2 1
           6 5 4
        */
        var s = ll(ll(1, 2, 3), ll(4, 5, 6));
        var fs = flipY(s);
        var exps = ll(ll(3, 2, 1), ll(6, 5, 4));
        t(fs, exps);
      }      
    }
    end();
  }

  public static void test_alignmentNonSPC(){
    beg();
    {
      {
        List<String> ls = ll(" x = y;");
        var lss         = alignmentNonSPC("=", ls);
        var expls       = ll(" x = y;");
        t(lss, expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             " x = y;");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll(" x = y;",
                       " x = y;");
        t(lss, expls);
        fl("lss");
        pp(lss);
        fl("expls");
        pp(expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             " x  = y;");
        var lss   = alignmentNonSPC("=", ls);
        var expls = ll(" x = y;",
                       " x = y;");
        t(lss, expls);
        fl("lss");
        pp(lss);
        fl("expls");
        pp(expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             "  x = y;");
        var lss   = alignmentNonSPC("=", ls);
        var expls = ll(" x = y;",
                       " x = y;");
        t(lss, expls);
      }
      {
        List<String> ls = ll(" x = y;",
                             "  x = yz;");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll(" x = y;",
                       " x = yz;");
        t(lss, expls);
      }
      {
        List<String> ls = ll("  x = y;",
                             " x = yz;");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll("  x = y;",
                       "  x = yz;");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x = y + u + v ",
                             " x = yz + u + v  ");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x  = y + u + v ",
                             " x = yz + u + v  ");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x  = y + u + v ",
                             " x  = yz + u + v  ");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x = y + u + v ",
                             " x  = yz + u + v  ");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x = y + u ",
                             " x  = yz + u + v  ");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll("x = y + u",
                       "x = yz + u + v");
        t(lss, expls);
      }
      {
        List<String> ls = ll("x =   y + u + v ",
                             " x  = yz  ");
        var lss = alignmentNonSPC("=", ls);
        var expls = ll("x = y + u + v",
                       "x = yz");
        t(lss, expls);
      }
    }
    end();
  }
  
  public static void test_tableMask_table_tableBit(){
    beg();
    {
      var ln = ll(1);
      var ls = table((x, y) -> tuple(x, y), ln, ln);
      var mask = tableBit((x, y) -> x == y ? 1 : 0, 3, 3);
      var ret = tableMask(ls, mask);
      t(ret, ll(tuple(1, 1)));
    }
    {
      var ln = ll(1);
      var ls = table((x, y) -> tuple(x, y), ln, ln);
      var mask = tableBit((x, y) -> x < y ? 1 : 0, 3, 3);
      var ret = tableMask(ls, mask);
      t(ret, ll());
    }
    {
      var ln = ll(1);
      var ls = table((x, y) -> tuple(x, y), ln, ln);
      var mask = tableBit((x, y) -> x > y ? 1 : 0, 3, 3);
      var ret = tableMask(ls, mask);
      t(ret, ll());
    }
    {
      var ln = ll(1, 2, 3);
      var ls = table((x, y) -> tuple(x, y), ln, ln);
      var mask = tableBit((x, y) -> x < y ? 1 : 0, 3, 3);
      var ret = tableMask(ls, mask);
      t(ret, ll(tuple(1, 2), tuple(1, 3), tuple(2, 3)));
    }
    {
      var ln = ll(1, 2, 3);
      var ls = table((x, y) -> tuple(x, y), ln, ln);
      var mask = tableBit((x, y) -> x == y ? 1 : 0, 3, 3);
      var ret = tableMask(ls, mask);
      t(ret, ll(tuple(1, 1), tuple(2, 2), tuple(3, 3)));
    }
    {
      var ln = ll(1, 2, 3);
      var ls = table((x, y) -> tuple(x, y), ln, ln);
      var mask = tableBit((x, y) -> x > y ? 1 : 0, 3, 3);
      var ret = tableMask(ls, mask);
      t(ret, ll(tuple(2, 1), tuple(3, 1), tuple(3, 2)));
    }
    end();
  }

  public static void test_joinListStr(){
	beg();
	{
	    {
		var ls = list("a", "b", "c");
		var ret  = joinListStr(ls);
		String expectedStr = "a\nb\nc";
		t(ret, expectedStr);
	    }
	}
	end();
  }
  
  public static void test_partitionWithCountSwap(){
	beg();
    {
        fl("partitionWithCountSwap 1");
        int[] arr = {1, 2, 0, 3, 0, 2, 5, 1, 1, 4, 3, 1};
        int len = arr.length;
        int lo = 0;
        int hi = len - 1;
        int[] expectedArr = {1, 0, 0, 1, 1, 1, 5, 3, 2, 4, 3, 2};
        Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
        t(t.x, 6);
        t(t.y, 6);
        t(expectedArr, arr);
    }
    {
        fl("partitionWithCountSwap 2");
        int[] arr = {2, 1};
        int len = arr.length;
        int lo = 0;
        int hi = len - 1;
        int[] expectedArr = {1, 2};
        Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
        t(t.x, 1);
        t(t.y, 1);
        t(expectedArr, arr);
    }
    {
        fl("partitionWithCountSwap 3");
        int[] arr = {2, 1, 1};
        int len = arr.length;
        int lo = 0;
        int hi = len - 1;
        int[] expectedArr = {1, 1, 2};
        Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
        t(t.x, 2);
        t(t.y, 2);
        t(expectedArr, arr);
    }
    {
        fl("partitionWithCountSwap 4");
        int[] arr = {1};
        int len = arr.length;
        int lo = 0;
        int hi = len - 1;
        Tuple<Integer, Integer> t = partitionWithCountSwap(arr, lo, hi);
        t(t.x, 0);
        t(t.y, 1);
    }
	end();
  }
  public static void test_partitionRev(){
    beg();
    {
        /**
           x
           0 2 1
           x
           
           x
           0 2 1
             x 
            
             x
           2 0 1
               x

             x 
           2 1 0
               x

        */
        fl("partitionRev 1");
        int[] arr = {0, 2, 1};
        int len = arr.length;
        int lo = 0;
        int hi = len - 1;
        int pivotInx = partitionRev(arr, lo, hi);
        int[] expectedArr = {2, 1, 0};
        t(pivotInx, 1);
        t(expectedArr, arr);
    }
    {
        fl("partitionRev 2");
        int[] arr = {1, 4, 2, 0, 1, 2, 3};
        int len = arr.length;
        int lo = 0;
        int hi = len - 1;
        int pivotInx = partitionRev(arr, lo, hi);
        int[] expectedArr = {4, 3, 2, 0, 1, 2, 1};
        t(pivotInx, 1);
        t(expectedArr, arr);
    }
    end();
  }

}
